# PI SETUP GUIDE #
1. Download [Raspbian Jessie lite](https://www.raspberrypi.org/downloads/raspbian/) torrent
2. Make an SD card with [Win32 Disc Imager](https://sourceforge.net/projects/win32diskimager/)
3. Connect PI to network with a cable, plug in SD card ant power. Wait for ~2mins.
4. Download [Putty](http://www.chiark.greenend.org.uk/~sgtatham/putty/download.html) and connect via local IP 168.192.0.X. User: **pi**, pass **raspberry**
5. Configure main options (navigating with **TAB**): ```sudo raspi-config``` 
**1. Expand Filesystem** -> **Enter** and reboot: ```sudo reboot```
6. Change main user to *username* for safety: ```sudo adduser username```. Enter enter enter, then give it sudo permissions: ```sudo visudo```. Add** *username* ALL=(ALL) NOPASSWD:ALL** at the bottom of the file. And save: **Ctrl + X** > **Y** > **Enter** 
7. Make it default on login (change *pi* to *username*): ```sudo nano /etc/systemd/system/autologin@.service```
8. Reboot
9. Delete *pi* user: ```sudo deluser -remove-home pi```
10. Update everything! ```sudo apt-get update -y && sudo apt-get upgrade -y```
####Enbale WiFi####
1. Open WiFi configuration: ```sudo nano /etc/wpa_supplicant/wpa_supplicant.conf```
2. Add following lines at the bottom of the file:
```
network={
    ssid="WiFiName"
    psk="WiFiPassword"
}
```
####VNC####
1. Install **tightvnc** ```sudo apt-get install tightvncserver -y```
2. Create a server instance ```vncserver :1``` - enter password and set NO to readonly password.
3. Enable VNC via ```sudo raspi-config``` **7. Advanced Options** > **A5 VNC** > **Yes**
4. Enable VNC autostart ```cd /home/username``` > ```cd .config``` > ```mkdir autostart``` > ```cd autostart``` > ```nano tightvnc.desktop``` and add these lines:
```
[Desktop Entry]
Type=Application
Name=TightVNC
Exec=vncserver :1
StartupNotify=false
```
####NodeJS####
1. Remove old node, is exists: ```sudo apt-get remove --purge node* npm* -y```
2. Remove dependencies: ```sudo apt-get autoremove -y```
3. This: ```curl -sL https://deb.nodesource.com/setup_7.x | sudo -E bash -``` from [here](http://thisdavej.com/beginners-guide-to-installing-node-js-on-a-raspberry-pi/)
4. And then ```sudo apt-get install nodejs -y```

###LAMP###
1. This: ```sudo apt-get install apache2 apache2-utils libapache2-mod-php5 php5 php-pear php5-xcache php5-mysql php5-curl php5-gd mysql-server mysql-client -y```